var express = require('express');
var app = express();
var port = process.env.PORT || 3001;
var mongoose = require('mongoose');
var Todo = require('./models/todo');
var User = require('./models/user');
var bodyParser = require('body-parser');

//connect to mongodb
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/todo');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./routes');
routes.todo(app);
routes.user(app);

app.listen(port);
console.log('App server started on: ' + port);