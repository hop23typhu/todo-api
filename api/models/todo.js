'use-strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TodoSchema = new Schema({
    name: {
        type: String,
        required: 'Field name is required'
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            emum: ['Pending', 'On-going', 'Completed']
        }],
        default: ['Completed']
    }
});

module.exports = mongoose.model('Todo', TodoSchema);