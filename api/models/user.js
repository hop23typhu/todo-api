'use-strict';
var mongoose = require('mongoose')

var Schema = mongoose.Schema;
var UserSchema = new Schema({
    name: {
        type: String,
        required: 'Field name is required'
    },
    fullname: {
        type: String,
        default: '',
    },
    create_at: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('User', UserSchema);