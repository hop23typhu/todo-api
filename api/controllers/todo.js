'use-strict';

var mongoose = require('mongoose')
Todo = mongoose.model('Todo');

exports.list_all = function (req, res) {
    Todo.find({}, function (err, todo) {
        if (err) res.send(err);
        res.json(todo);
    });
};

exports.create = function (req, res) {
    var new_todo = new Todo(req.body);
    new_todo.save(function (err, todo) {
        if (err) res.send(err);
        res.json(todo);
    });
};

exports.list_one = function (req, res) {
    Todo.findById(req.params.ID, function (err, todo) {
        if (err)
            res.send(err);
        res.json(todo);
    });
};

exports.update = function (req, res) {
    Todo.findOneAndUpdate({ _id: req.params.ID }, req.body, { new: true }, function (err, todo) {
        if (err)
            res.send(err);
        res.json(todo);
    });
};

exports.delete = function (req, res) {

    Todo.remove({
        _id: req.params.ID
    }, function (err, todo) {
        if (err)
            res.send(err);
        res.json({ message: 'Todo successfully deleted' });
    });
};