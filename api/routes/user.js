'use-strict';
module.exports = function (app) {
    var userCtrl = require('../controllers/user');

    app.route('/user')
        .get(userCtrl.get_all)
        .put(userCtrl.create)
};