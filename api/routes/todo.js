'use-strict';
module.exports = function (app) {
    var todoCtrl = require('../controllers/todo');

    app.route('/todo')
        .get(todoCtrl.list_all)
        .post(todoCtrl.create);


    app.route('/todo/:ID')
        .get(todoCtrl.list_one)
        .put(todoCtrl.update)
        .delete(todoCtrl.delete);
};